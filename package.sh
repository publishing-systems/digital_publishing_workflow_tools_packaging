#!/bin/sh
# Copyright (C) 2017-2023 Stephan Kreutzer
#
# This file is part of digital_publishing_workflow_tools_packaging.
#
# digital_publishing_workflow_tools_packaging is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3 or any later version,
# as published by the Free Software Foundation.
#
# digital_publishing_workflow_tools_packaging is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License 3 for more details.
#
# You should have received a copy of the GNU Affero General Public License 3
# along with digital_publishing_workflow_tools_packaging. If not, see <http://www.gnu.org/licenses/>.

rm -rf .git

git clone https://gitlab.com/publishing-systems/jterosta.git
cd ./jterosta/
rm -rf .git
# Consider the JDK version (target framework), as versions are forward-compatible,
# but not backward-compatible!
make
cd ..

git clone https://gitlab.com/publishing-systems/tero-grammars.git
cd ./tero-grammars/
rm -rf .git
cd ..

git clone https://gitlab.com/publishing-systems/digital_publishing_workflow_tools.git
cd digital_publishing_workflow_tools
rm -rf .git
# Consider the JDK version (target framework), as versions are forward-compatible,
# but not backward-compatible!
make
cd ..

cp -r ./jterosta/org/ ./digital_publishing_workflow_tools/htx/gui/viewer/viewer_1/
cd ./digital_publishing_workflow_tools/
cd ./htx/
cd ./gui/
cd ./viewer/
cd ./viewer_1/
make
cd ..
cd ..
cd ..
cd ..
cd ..

cp -r ./jterosta/org/ ./digital_publishing_workflow_tools/htx/gui/viewer/viewer_2/
cd ./digital_publishing_workflow_tools/
cd ./htx/
cd ./gui/
cd ./viewer/
cd ./viewer_2/
make
cd ..
cd ..
cd ..
cd ..
cd ..

cp -r ./jterosta/org/ ./digital_publishing_workflow_tools/htx/gui/viewer/viewer_3/
cd ./digital_publishing_workflow_tools/
cd ./htx/
cd ./gui/
cd ./viewer/
cd ./viewer_3/
make
cd ..
cd ..
cd ..
cd ..
cd ..

cp -r ./jterosta/org/ ./digital_publishing_workflow_tools/htx/gui/interactive_tero/interactive_tero_1/
cp -r ./tero-grammars/ ./digital_publishing_workflow_tools/htx/gui/interactive_tero/interactive_tero_1/
cd ./digital_publishing_workflow_tools/
cd ./htx/
cd ./gui/
cd ./interactive_tero/
cd ./interactive_tero_1/
make
cd ..
cd ..
cd ..
cd ..
cd ..


printf "Build date: $(date "+%Y-%m-%d").\n" > ./digital_publishing_workflow_tools/version.txt
currentDate=$(date "+%Y%m%d")

cp -r digital_publishing_workflow_tools digital_publishing_workflow_tools_gnu
cp -r -n scripts_gnu/* digital_publishing_workflow_tools_gnu
zip -r digital_publishing_workflow_tools_gnu_$currentDate.zip digital_publishing_workflow_tools_gnu
sha256sum digital_publishing_workflow_tools_gnu_$currentDate.zip > digital_publishing_workflow_tools_gnu_$currentDate.zip.sha256
rm -r digital_publishing_workflow_tools_gnu

cp -r digital_publishing_workflow_tools digital_publishing_workflow_tools_windows
cp -r -n scripts_windows/* digital_publishing_workflow_tools_windows
zip -r digital_publishing_workflow_tools_windows_$currentDate.zip digital_publishing_workflow_tools_windows
sha256sum digital_publishing_workflow_tools_windows_$currentDate.zip > digital_publishing_workflow_tools_windows_$currentDate.zip.sha256
rm -r digital_publishing_workflow_tools_windows



java -cp ./digital_publishing_workflow_tools/workflows/setup/setup_1/ setup_1

mkdir -p ./change_tracking_text_editor_1_gnu/
cp ./digital_publishing_workflow_tools/AUTHORS ./change_tracking_text_editor_1_gnu/
cp ./digital_publishing_workflow_tools/LICENSE ./change_tracking_text_editor_1_gnu/
mkdir -p ./change_tracking_text_editor_1_gnu/htx/gui/change_tracking_text_editor/change_tracking_text_editor_1/
cp -r ./digital_publishing_workflow_tools/htx/gui/change_tracking_text_editor/change_tracking_text_editor_1/* ./change_tracking_text_editor_1_gnu/htx/gui/change_tracking_text_editor/change_tracking_text_editor_1/
cp -r -n ./scripts_gnu/htx/gui/change_tracking_text_editor/change_tracking_text_editor_1/* ./change_tracking_text_editor_1_gnu/htx/gui/change_tracking_text_editor/change_tracking_text_editor_1/
mkdir -p ./change_tracking_text_editor_1_gnu/htx/workflows/change_tracking_text_editor/change_tracking_text_editor_1/
cp -r ./digital_publishing_workflow_tools/htx/workflows/change_tracking_text_editor/change_tracking_text_editor_1/* ./change_tracking_text_editor_1_gnu/htx/workflows/change_tracking_text_editor/change_tracking_text_editor_1/
cp -r -n ./scripts_gnu/htx/workflows/change_tracking_text_editor/change_tracking_text_editor_1/* ./change_tracking_text_editor_1_gnu/htx/workflows/change_tracking_text_editor/change_tracking_text_editor_1/
mkdir -p ./change_tracking_text_editor_1_gnu/htx/change_history_concatenator/change_history_concatenator_1/
cp -r ./digital_publishing_workflow_tools/htx/change_history_concatenator/change_history_concatenator_1/* ./change_tracking_text_editor_1_gnu/htx/change_history_concatenator/change_history_concatenator_1/
cp -r -n ./scripts_gnu/htx/change_history_concatenator/change_history_concatenator_1/* ./change_tracking_text_editor_1_gnu/htx/change_history_concatenator/change_history_concatenator_1/
mkdir -p ./change_tracking_text_editor_1_gnu/htx/change_instructions_executor/change_instructions_executor_1/
cp -r ./digital_publishing_workflow_tools/htx/change_instructions_executor/change_instructions_executor_1/* ./change_tracking_text_editor_1_gnu/htx/change_instructions_executor/change_instructions_executor_1/
cp -r -n ./scripts_gnu/htx/change_instructions_executor/change_instructions_executor_1/* ./change_tracking_text_editor_1_gnu/htx/change_instructions_executor/change_instructions_executor_1/
mkdir -p ./change_tracking_text_editor_1_gnu/htx/change_instructions_concatenator/change_instructions_concatenator_1/
cp -r ./digital_publishing_workflow_tools/htx/change_instructions_concatenator/change_instructions_concatenator_1/* ./change_tracking_text_editor_1_gnu/htx/change_instructions_concatenator/change_instructions_concatenator_1/
cp -r -n ./scripts_gnu/htx/change_instructions_concatenator/change_instructions_concatenator_1/* ./change_tracking_text_editor_1_gnu/htx/change_instructions_concatenator/change_instructions_concatenator_1/
mkdir -p ./change_tracking_text_editor_1_gnu/htx/change_instructions_optimizer/change_instructions_optimizer_1/
cp -r ./digital_publishing_workflow_tools/htx/change_instructions_optimizer/change_instructions_optimizer_1/* ./change_tracking_text_editor_1_gnu/htx/change_instructions_optimizer/change_instructions_optimizer_1/
cp -r -n ./scripts_gnu/htx/change_instructions_optimizer/change_instructions_optimizer_1/* ./change_tracking_text_editor_1_gnu/htx/change_instructions_optimizer/change_instructions_optimizer_1/
mkdir -p ./change_tracking_text_editor_1_gnu/xml_xslt_transformator/xml_xslt_transformator_1/
cp -r ./digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/* ./change_tracking_text_editor_1_gnu/xml_xslt_transformator/xml_xslt_transformator_1/
cp -r -n ./scripts_gnu/xml_xslt_transformator/xml_xslt_transformator_1/* ./change_tracking_text_editor_1_gnu/xml_xslt_transformator/xml_xslt_transformator_1/
mkdir -p ./change_tracking_text_editor_1_gnu/gui/file_picker/file_picker_1/
cp -r ./digital_publishing_workflow_tools/gui/file_picker/file_picker_1/* ./change_tracking_text_editor_1_gnu/gui/file_picker/file_picker_1/
mkdir -p ./change_tracking_text_editor_1_gnu/gui/option_picker/option_picker_1/
cp -r ./digital_publishing_workflow_tools/gui/option_picker/option_picker_1/* ./change_tracking_text_editor_1_gnu/gui/option_picker/option_picker_1/
cp -r -f ./change_tracking_text_editor_1_scripts_gnu/* ./change_tracking_text_editor_1_gnu/
zip -r ./change_tracking_text_editor_1_gnu_$currentDate.zip ./change_tracking_text_editor_1_gnu/
sha256sum ./change_tracking_text_editor_1_gnu_$currentDate.zip > ./change_tracking_text_editor_1_gnu_$currentDate.zip.sha256
rm -r ./change_tracking_text_editor_1_gnu/

mkdir -p ./change_tracking_text_editor_1_windows/
cp ./digital_publishing_workflow_tools/AUTHORS ./change_tracking_text_editor_1_windows/
cp ./digital_publishing_workflow_tools/LICENSE ./change_tracking_text_editor_1_windows/
mkdir -p ./change_tracking_text_editor_1_windows/htx/gui/change_tracking_text_editor/change_tracking_text_editor_1/
cp -r ./digital_publishing_workflow_tools/htx/gui/change_tracking_text_editor/change_tracking_text_editor_1/* ./change_tracking_text_editor_1_windows/htx/gui/change_tracking_text_editor/change_tracking_text_editor_1/
cp -r -n ./scripts_windows/htx/gui/change_tracking_text_editor/change_tracking_text_editor_1/* ./change_tracking_text_editor_1_windows/htx/gui/change_tracking_text_editor/change_tracking_text_editor_1/
mkdir -p ./change_tracking_text_editor_1_windows/htx/workflows/change_tracking_text_editor/change_tracking_text_editor_1/
cp -r ./digital_publishing_workflow_tools/htx/workflows/change_tracking_text_editor/change_tracking_text_editor_1/* ./change_tracking_text_editor_1_windows/htx/workflows/change_tracking_text_editor/change_tracking_text_editor_1/
cp -r -n ./scripts_windows/htx/workflows/change_tracking_text_editor/change_tracking_text_editor_1/* ./change_tracking_text_editor_1_windows/htx/workflows/change_tracking_text_editor/change_tracking_text_editor_1/
mkdir -p ./change_tracking_text_editor_1_windows/htx/change_history_concatenator/change_history_concatenator_1/
cp -r ./digital_publishing_workflow_tools/htx/change_history_concatenator/change_history_concatenator_1/* ./change_tracking_text_editor_1_windows/htx/change_history_concatenator/change_history_concatenator_1/
cp -r -n ./scripts_windows/htx/change_history_concatenator/change_history_concatenator_1/* ./change_tracking_text_editor_1_windows/htx/change_history_concatenator/change_history_concatenator_1/
mkdir -p change_tracking_text_editor_1_windows/htx/change_instructions_executor/change_instructions_executor_1/
cp -r ./digital_publishing_workflow_tools/htx/change_instructions_executor/change_instructions_executor_1/* ./change_tracking_text_editor_1_windows/htx/change_instructions_executor/change_instructions_executor_1/
cp -r -n ./scripts_windows/htx/change_instructions_executor/change_instructions_executor_1/* ./change_tracking_text_editor_1_windows/htx/change_instructions_executor/change_instructions_executor_1/
mkdir -p ./change_tracking_text_editor_1_windows/htx/change_instructions_concatenator/change_instructions_concatenator_1/
cp -r ./digital_publishing_workflow_tools/htx/change_instructions_concatenator/change_instructions_concatenator_1/* ./change_tracking_text_editor_1_windows/htx/change_instructions_concatenator/change_instructions_concatenator_1/
cp -r -n ./scripts_windows/htx/change_instructions_concatenator/change_instructions_concatenator_1/* ./change_tracking_text_editor_1_windows/htx/change_instructions_concatenator/change_instructions_concatenator_1/
mkdir -p ./change_tracking_text_editor_1_windows/htx/change_instructions_optimizer/change_instructions_optimizer_1/
cp -r ./digital_publishing_workflow_tools/htx/change_instructions_optimizer/change_instructions_optimizer_1/* ./change_tracking_text_editor_1_windows/htx/change_instructions_optimizer/change_instructions_optimizer_1/
cp -r -n ./scripts_windows/htx/change_instructions_optimizer/change_instructions_optimizer_1/* ./change_tracking_text_editor_1_windows/htx/change_instructions_optimizer/change_instructions_optimizer_1/
mkdir -p ./change_tracking_text_editor_1_windows/xml_xslt_transformator/xml_xslt_transformator_1/
cp -r ./digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/* ./change_tracking_text_editor_1_windows/xml_xslt_transformator/xml_xslt_transformator_1/
cp -r -n ./scripts_windows/xml_xslt_transformator/xml_xslt_transformator_1/* ./change_tracking_text_editor_1_windows/xml_xslt_transformator/xml_xslt_transformator_1/
mkdir -p ./change_tracking_text_editor_1_windows/gui/file_picker/file_picker_1/
cp -r ./digital_publishing_workflow_tools/gui/file_picker/file_picker_1/* ./change_tracking_text_editor_1_windows/gui/file_picker/file_picker_1/
mkdir -p ./change_tracking_text_editor_1_windows/gui/option_picker/option_picker_1/
cp -r ./digital_publishing_workflow_tools/gui/option_picker/option_picker_1/* ./change_tracking_text_editor_1_windows/gui/option_picker/option_picker_1/
cp -r -f ./change_tracking_text_editor_1_scripts_windows/* ./change_tracking_text_editor_1_windows/
zip -r ./change_tracking_text_editor_1_windows_$currentDate.zip ./change_tracking_text_editor_1_windows/
sha256sum ./change_tracking_text_editor_1_windows_$currentDate.zip > ./change_tracking_text_editor_1_windows_$currentDate.zip.sha256
rm -r ./change_tracking_text_editor_1_windows/


mkdir -p ./xhtml_to_glossary_1_gnu/workflows/xhtml_to_glossary/xhtml_to_glossary_1/
cp ./digital_publishing_workflow_tools/AUTHORS ./xhtml_to_glossary_1_gnu/
cp ./digital_publishing_workflow_tools/LICENSE ./xhtml_to_glossary_1_gnu/
cp -r ./digital_publishing_workflow_tools/workflows/xhtml_to_glossary/xhtml_to_glossary_1/* ./xhtml_to_glossary_1_gnu/workflows/xhtml_to_glossary/xhtml_to_glossary_1/
cp -r -n ./scripts_gnu/workflows/xhtml_to_glossary/xhtml_to_glossary_1/* ./xhtml_to_glossary_1_gnu/workflows/xhtml_to_glossary/xhtml_to_glossary_1/
cp -r -f ./xhtml_to_glossary_1_scripts_gnu/* ./xhtml_to_glossary_1_gnu/
mkdir -p ./xhtml_to_glossary_1_gnu/gui/file_picker/file_picker_1/
cp -r ./digital_publishing_workflow_tools/gui/file_picker/file_picker_1/* ./xhtml_to_glossary_1_gnu/gui/file_picker/file_picker_1/
mkdir -p ./xhtml_to_glossary_1_gnu/xml_xslt_transformator/xml_xslt_transformator_1/
cp -r ./digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/* ./xhtml_to_glossary_1_gnu/xml_xslt_transformator/xml_xslt_transformator_1/
cp -r -n ./scripts_gnu/xml_xslt_transformator/xml_xslt_transformator_1/* ./xhtml_to_glossary_1_gnu/xml_xslt_transformator/xml_xslt_transformator_1/
mkdir -p ./xhtml_to_glossary_1_gnu/xml_concatenator/xml_concatenator_1/
cp -r ./digital_publishing_workflow_tools/xml_concatenator/xml_concatenator_1/* ./xhtml_to_glossary_1_gnu/xml_concatenator/xml_concatenator_1/
cp -r -n ./scripts_gnu/xml_concatenator/xml_concatenator_1/* ./xhtml_to_glossary_1_gnu/xml_concatenator/xml_concatenator_1/
zip -r xhtml_to_glossary_1_gnu_$currentDate.zip xhtml_to_glossary_1_gnu
sha256sum xhtml_to_glossary_1_gnu_$currentDate.zip > xhtml_to_glossary_1_gnu_$currentDate.zip.sha256
rm -r xhtml_to_glossary_1_gnu

mkdir -p ./xhtml_to_glossary_1_windows/workflows/xhtml_to_glossary/xhtml_to_glossary_1/
cp ./digital_publishing_workflow_tools/AUTHORS ./xhtml_to_glossary_1_windows/
cp ./digital_publishing_workflow_tools/LICENSE ./xhtml_to_glossary_1_windows/
cp -r ./digital_publishing_workflow_tools/workflows/xhtml_to_glossary/xhtml_to_glossary_1/* ./xhtml_to_glossary_1_windows/workflows/xhtml_to_glossary/xhtml_to_glossary_1/
cp -r -n ./scripts_windows/workflows/xhtml_to_glossary/xhtml_to_glossary_1/* ./xhtml_to_glossary_1_windows/workflows/xhtml_to_glossary/xhtml_to_glossary_1/
cp -r -f ./xhtml_to_glossary_1_scripts_windows/* ./xhtml_to_glossary_1_windows/
mkdir -p ./xhtml_to_glossary_1_windows/gui/file_picker/file_picker_1/
cp -r ./digital_publishing_workflow_tools/gui/file_picker/file_picker_1/* ./xhtml_to_glossary_1_windows/gui/file_picker/file_picker_1/
mkdir -p ./xhtml_to_glossary_1_windows/xml_xslt_transformator/xml_xslt_transformator_1/
cp -r ./digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/* ./xhtml_to_glossary_1_windows/xml_xslt_transformator/xml_xslt_transformator_1/
cp -r -n ./scripts_windows/xml_xslt_transformator/xml_xslt_transformator_1/* ./xhtml_to_glossary_1_windows/xml_xslt_transformator/xml_xslt_transformator_1/
mkdir -p ./xhtml_to_glossary_1_windows/xml_concatenator/xml_concatenator_1/
cp -r ./digital_publishing_workflow_tools/xml_concatenator/xml_concatenator_1/* ./xhtml_to_glossary_1_windows/xml_concatenator/xml_concatenator_1/
cp -r -n ./scripts_windows/xml_concatenator/xml_concatenator_1/* ./xhtml_to_glossary_1_windows/xml_concatenator/xml_concatenator_1/
zip -r xhtml_to_glossary_1_windows_$currentDate.zip xhtml_to_glossary_1_windows
sha256sum xhtml_to_glossary_1_windows_$currentDate.zip > xhtml_to_glossary_1_windows_$currentDate.zip.sha256
rm -r xhtml_to_glossary_1_windows


mkdir -p ./hyperdex_1_gnu/htx/gui/hyperdex/hyperdex_1/
cp ./digital_publishing_workflow_tools/AUTHORS ./hyperdex_1_gnu/
cp ./digital_publishing_workflow_tools/LICENSE ./hyperdex_1_gnu/
cp -r ./digital_publishing_workflow_tools/htx/gui/hyperdex/hyperdex_1/* ./hyperdex_1_gnu/htx/gui/hyperdex/hyperdex_1/
cp -r -n ./scripts_gnu/htx/gui/hyperdex/hyperdex_1/* ./hyperdex_1_gnu/htx/gui/hyperdex/hyperdex_1/
cp -r -f ./hyperdex_1_scripts_gnu/* ./hyperdex_1_gnu/
mkdir -p ./hyperdex_1_gnu/xml_to_dimgra/xml_to_dimgra_1/
cp -r ./digital_publishing_workflow_tools/xml_to_dimgra/xml_to_dimgra_1/* ./hyperdex_1_gnu/xml_to_dimgra/xml_to_dimgra_1/
cp -r -n ./scripts_gnu/xml_to_dimgra/xml_to_dimgra_1/* ./hyperdex_1_gnu/xml_to_dimgra/xml_to_dimgra_1/
zip -r hyperdex_1_gnu_$currentDate.zip hyperdex_1_gnu
sha256sum hyperdex_1_gnu_$currentDate.zip > hyperdex_1_gnu_$currentDate.zip.sha256
rm -r ./hyperdex_1_gnu/

mkdir -p ./hyperdex_1_windows/htx/gui/hyperdex/hyperdex_1/
cp ./digital_publishing_workflow_tools/AUTHORS ./hyperdex_1_windows/
cp ./digital_publishing_workflow_tools/LICENSE ./hyperdex_1_windows/
cp -r ./digital_publishing_workflow_tools/htx/gui/hyperdex/hyperdex_1/* ./hyperdex_1_windows/htx/gui/hyperdex/hyperdex_1/
cp -r -n ./scripts_windows/htx/gui/hyperdex/hyperdex_1/* ./hyperdex_1_windows/htx/gui/hyperdex/hyperdex_1/
cp -r -f ./hyperdex_1_scripts_windows/* ./hyperdex_1_windows/
mkdir -p ./hyperdex_1_windows/xml_to_dimgra/xml_to_dimgra_1/
cp -r ./digital_publishing_workflow_tools/xml_to_dimgra/xml_to_dimgra_1/* ./hyperdex_1_windows/xml_to_dimgra/xml_to_dimgra_1/
cp -r -n ./scripts_windows/xml_to_dimgra/xml_to_dimgra_1/* ./hyperdex_1_windows/xml_to_dimgra/xml_to_dimgra_1/
zip -r hyperdex_1_windows_$currentDate.zip hyperdex_1_windows
sha256sum hyperdex_1_windows_$currentDate.zip > hyperdex_1_windows_$currentDate.zip.sha256
rm -r ./hyperdex_1_windows/


mkdir -p ./viewer_1_gnu/htx/gui/viewer/viewer_1/
cp ./digital_publishing_workflow_tools/AUTHORS ./viewer_1_gnu/
cp ./digital_publishing_workflow_tools/LICENSE ./viewer_1_gnu/
cp -r ./digital_publishing_workflow_tools/htx/gui/viewer/viewer_1/* ./viewer_1_gnu/htx/gui/viewer/viewer_1/
cp -r -n ./scripts_gnu/htx/gui/viewer/viewer_1/* ./viewer_1_gnu/htx/gui/viewer/viewer_1/
cp -r -f ./viewer_1_scripts_gnu/* ./viewer_1_gnu/
mkdir -p ./viewer_1_gnu/workflows/resource_retriever/resource_retriever_1/
cp -r ./digital_publishing_workflow_tools/workflows/resource_retriever/resource_retriever_1/* ./viewer_1_gnu/workflows/resource_retriever/resource_retriever_1/
cp -r -n ./scripts_gnu/workflows/resource_retriever/resource_retriever_1/* ./viewer_1_gnu/workflows/resource_retriever/resource_retriever_1/
mkdir -p ./viewer_1_gnu/http_client/http_client_1/
cp -r ./digital_publishing_workflow_tools/http_client/http_client_1/* ./viewer_1_gnu/http_client/http_client_1/
cp -r -n ./scripts_gnu/http_client/http_client_1/* ./viewer_1_gnu/http_client/http_client_1/
mkdir -p ./viewer_1_gnu/https_client/https_client_1/
cp -r ./digital_publishing_workflow_tools/https_client/https_client_1/* ./viewer_1_gnu/https_client/https_client_1/
cp -r -n ./scripts_gnu/https_client/https_client_1/* ./viewer_1_gnu/https_client/https_client_1/
zip -r viewer_1_gnu_$currentDate.zip viewer_1_gnu
sha256sum viewer_1_gnu_$currentDate.zip > viewer_1_gnu_$currentDate.zip.sha256
rm -r ./viewer_1_gnu/

mkdir -p ./viewer_1_windows/htx/gui/viewer/viewer_1/
cp ./digital_publishing_workflow_tools/AUTHORS ./viewer_1_windows/
cp ./digital_publishing_workflow_tools/LICENSE ./viewer_1_windows/
cp -r ./digital_publishing_workflow_tools/htx/gui/viewer/viewer_1/* ./viewer_1_windows/htx/gui/viewer/viewer_1/
cp -r -n ./scripts_windows/htx/gui/viewer/viewer_1/* ./viewer_1_windows/htx/gui/viewer/viewer_1/
cp -r -f ./viewer_1_scripts_windows/* ./viewer_1_windows/
mkdir -p ./viewer_1_windows/workflows/resource_retriever/resource_retriever_1/
cp -r ./digital_publishing_workflow_tools/workflows/resource_retriever/resource_retriever_1/* ./viewer_1_windows/workflows/resource_retriever/resource_retriever_1/
cp -r -n ./scripts_windows/workflows/resource_retriever/resource_retriever_1/* ./viewer_1_windows/workflows/resource_retriever/resource_retriever_1/
mkdir -p ./viewer_1_windows/http_client/http_client_1/
cp -r ./digital_publishing_workflow_tools/http_client/http_client_1/* ./viewer_1_windows/http_client/http_client_1/
cp -r -n ./scripts_windows/http_client/http_client_1/* ./viewer_1_windows/http_client/http_client_1/
mkdir -p ./viewer_1_windows/https_client/https_client_1/
cp -r ./digital_publishing_workflow_tools/https_client/https_client_1/* ./viewer_1_windows/https_client/https_client_1/
cp -r -n ./scripts_windows/https_client/https_client_1/* ./viewer_1_windows/https_client/https_client_1/
zip -r viewer_1_windows_$currentDate.zip viewer_1_windows
sha256sum viewer_1_windows_$currentDate.zip > viewer_1_windows_$currentDate.zip.sha256
rm -r ./viewer_1_windows/


mkdir -p ./viewer_2_gnu/htx/gui/viewer/viewer_2/
cp ./digital_publishing_workflow_tools/AUTHORS ./viewer_2_gnu/
cp ./digital_publishing_workflow_tools/LICENSE ./viewer_2_gnu/
cp -r ./digital_publishing_workflow_tools/htx/gui/viewer/viewer_2/* ./viewer_2_gnu/htx/gui/viewer/viewer_2/
cp -r -n ./scripts_gnu/htx/gui/viewer/viewer_2/* ./viewer_2_gnu/htx/gui/viewer/viewer_2/
cp -r -f ./viewer_2_scripts_gnu/* ./viewer_2_gnu/
mkdir -p ./viewer_2_gnu/workflows/resource_retriever/resource_retriever_1/
cp -r ./digital_publishing_workflow_tools/workflows/resource_retriever/resource_retriever_1/* ./viewer_2_gnu/workflows/resource_retriever/resource_retriever_1/
cp -r -n ./scripts_gnu/workflows/resource_retriever/resource_retriever_1/* ./viewer_2_gnu/workflows/resource_retriever/resource_retriever_1/
mkdir -p ./viewer_2_gnu/http_client/http_client_1/
cp -r ./digital_publishing_workflow_tools/http_client/http_client_1/* ./viewer_2_gnu/http_client/http_client_1/
cp -r -n ./scripts_gnu/http_client/http_client_1/* ./viewer_2_gnu/http_client/http_client_1/
mkdir -p ./viewer_2_gnu/https_client/https_client_1/
cp -r ./digital_publishing_workflow_tools/https_client/https_client_1/* ./viewer_2_gnu/https_client/https_client_1/
cp -r -n ./scripts_gnu/https_client/https_client_1/* ./viewer_2_gnu/https_client/https_client_1/
zip -r viewer_2_gnu_$currentDate.zip viewer_2_gnu
sha256sum viewer_2_gnu_$currentDate.zip > viewer_2_gnu_$currentDate.zip.sha256
rm -r ./viewer_2_gnu/

mkdir -p ./viewer_2_windows/htx/gui/viewer/viewer_2/
cp ./digital_publishing_workflow_tools/AUTHORS ./viewer_2_windows/
cp ./digital_publishing_workflow_tools/LICENSE ./viewer_2_windows/
cp -r ./digital_publishing_workflow_tools/htx/gui/viewer/viewer_2/* ./viewer_2_windows/htx/gui/viewer/viewer_2/
cp -r -n ./scripts_windows/htx/gui/viewer/viewer_2/* ./viewer_2_windows/htx/gui/viewer/viewer_2/
cp -r -f ./viewer_2_scripts_windows/* ./viewer_2_windows/
mkdir -p ./viewer_2_windows/workflows/resource_retriever/resource_retriever_1/
cp -r ./digital_publishing_workflow_tools/workflows/resource_retriever/resource_retriever_1/* ./viewer_2_windows/workflows/resource_retriever/resource_retriever_1/
cp -r -n ./scripts_windows/workflows/resource_retriever/resource_retriever_1/* ./viewer_2_windows/workflows/resource_retriever/resource_retriever_1/
mkdir -p ./viewer_2_windows/http_client/http_client_1/
cp -r ./digital_publishing_workflow_tools/http_client/http_client_1/* ./viewer_2_windows/http_client/http_client_1/
cp -r -n ./scripts_windows/http_client/http_client_1/* ./viewer_2_windows/http_client/http_client_1/
mkdir -p ./viewer_2_windows/https_client/https_client_1/
cp -r ./digital_publishing_workflow_tools/https_client/https_client_1/* ./viewer_2_windows/https_client/https_client_1/
cp -r -n ./scripts_windows/https_client/https_client_1/* ./viewer_2_windows/https_client/https_client_1/
zip -r viewer_2_windows_$currentDate.zip viewer_2_windows
sha256sum viewer_2_windows_$currentDate.zip > viewer_2_windows_$currentDate.zip.sha256
rm -r ./viewer_2_windows/


mkdir -p ./htx_1_gnu/htx/workflows/htx/htx_1/
cp ./digital_publishing_workflow_tools/AUTHORS ./htx_1_gnu/
cp ./digital_publishing_workflow_tools/LICENSE ./htx_1_gnu/
cp -r ./digital_publishing_workflow_tools/htx/workflows/htx/htx_1/* ./htx_1_gnu/htx/workflows/htx/htx_1/
cp -r -n ./scripts_gnu/htx/workflows/htx/htx_1/* ./htx_1_gnu/htx/workflows/htx/htx_1/
cp -r -f ./htx_1_scripts_gnu/* ./htx_1_gnu/
mkdir -p ./htx_1_gnu/htx/gui/viewer/viewer_3/
cp -r ./digital_publishing_workflow_tools/htx/gui/viewer/viewer_3/* ./htx_1_gnu/htx/gui/viewer/viewer_3/
cp -r -n ./scripts_gnu/htx/gui/viewer/viewer_3/* ./htx_1_gnu/htx/gui/viewer/viewer_3/
mkdir -p ./htx_1_gnu/htx/gui/hyperdex/hyperdex_1/
cp -r ./digital_publishing_workflow_tools/htx/gui/hyperdex/hyperdex_1/* ./htx_1_gnu/htx/gui/hyperdex/hyperdex_1/
cp -r -n ./scripts_gnu/htx/gui/hyperdex/hyperdex_1/* ./htx_1_gnu/htx/gui/hyperdex/hyperdex_1/
mkdir -p ./htx_1_gnu/workflows/storage_resource_retriever/storage_resource_retriever_1/
cp -r ./digital_publishing_workflow_tools/workflows/storage_resource_retriever/storage_resource_retriever_1/* ./htx_1_gnu/workflows/storage_resource_retriever/storage_resource_retriever_1/
cp -r -n ./scripts_gnu/workflows/storage_resource_retriever/storage_resource_retriever_1/* ./htx_1_gnu/workflows/storage_resource_retriever/storage_resource_retriever_1/
mkdir -p ./htx_1_gnu/workflows/resource_retriever/resource_retriever_1/
cp -r ./digital_publishing_workflow_tools/workflows/resource_retriever/resource_retriever_1/* ./htx_1_gnu/workflows/resource_retriever/resource_retriever_1/
cp -r -n ./scripts_gnu/workflows/resource_retriever/resource_retriever_1/* ./htx_1_gnu/workflows/resource_retriever/resource_retriever_1/
mkdir -p ./htx_1_gnu/http_client/http_client_1/
cp -r ./digital_publishing_workflow_tools/http_client/http_client_1/* ./htx_1_gnu/http_client/http_client_1/
cp -r -n ./scripts_gnu/http_client/http_client_1/* ./htx_1_gnu/http_client/http_client_1/
mkdir -p ./htx_1_gnu/https_client/https_client_1/
cp -r ./digital_publishing_workflow_tools/https_client/https_client_1/* ./htx_1_gnu/https_client/https_client_1/
cp -r -n ./scripts_gnu/https_client/https_client_1/* ./htx_1_gnu/https_client/https_client_1/
mkdir -p ./htx_1_gnu/xml_namespace_extractor/xml_namespace_extractor_1/
cp -r ./digital_publishing_workflow_tools/xml_namespace_extractor/xml_namespace_extractor_1/* ./htx_1_gnu/xml_namespace_extractor/xml_namespace_extractor_1/
cp -r -n ./scripts_gnu/xml_namespace_extractor/xml_namespace_extractor_1/* ./htx_1_gnu/xml_namespace_extractor/xml_namespace_extractor_1/
mkdir -p ./htx_1_gnu/xml_xslt_transformator/xml_xslt_transformator_1/
cp -r ./digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/* ./htx_1_gnu/xml_xslt_transformator/xml_xslt_transformator_1/
cp -r -n ./scripts_gnu/xml_xslt_transformator/xml_xslt_transformator_1/* ./htx_1_gnu/xml_xslt_transformator/xml_xslt_transformator_1/
mkdir -p ./htx_1_gnu/gui/list_picker/list_picker_1/
cp -r ./digital_publishing_workflow_tools/gui/list_picker/list_picker_1/* ./htx_1_gnu/gui/list_picker/list_picker_1/
cp -r ./digital_publishing_workflow_tools/workflows/storage_resource_retriever/storage_resource_retriever_1/* ./htx_1_gnu/workflows/storage_resource_retriever/storage_resource_retriever_1/
zip -r htx_1_gnu_$currentDate.zip htx_1_gnu
sha256sum htx_1_gnu_$currentDate.zip > htx_1_gnu_$currentDate.zip.sha256
rm -r ./htx_1_gnu/

mkdir -p ./htx_1_windows/htx/workflows/htx/htx_1/
cp ./digital_publishing_workflow_tools/AUTHORS ./htx_1_windows/
cp ./digital_publishing_workflow_tools/LICENSE ./htx_1_windows/
cp -r ./digital_publishing_workflow_tools/htx/workflows/htx/htx_1/* ./htx_1_windows/htx/workflows/htx/htx_1/
cp -r -n ./scripts_windows/htx/workflows/htx/htx_1/* ./htx_1_windows/htx/workflows/htx/htx_1/
cp -r -f ./htx_1_scripts_windows/* ./htx_1_windows/
mkdir -p ./htx_1_windows/htx/gui/viewer/viewer_3/
cp -r ./digital_publishing_workflow_tools/htx/gui/viewer/viewer_3/* ./htx_1_windows/htx/gui/viewer/viewer_3/
cp -r -n ./scripts_windows/htx/gui/viewer/viewer_3/* ./htx_1_windows/htx/gui/viewer/viewer_3/
mkdir -p ./htx_1_windows/htx/gui/hyperdex/hyperdex_1/
cp -r ./digital_publishing_workflow_tools/htx/gui/hyperdex/hyperdex_1/* ./htx_1_windows/htx/gui/hyperdex/hyperdex_1/
cp -r -n ./scripts_windows/htx/gui/hyperdex/hyperdex_1/* ./htx_1_windows/htx/gui/hyperdex/hyperdex_1/
mkdir -p ./htx_1_windows/workflows/storage_resource_retriever/storage_resource_retriever_1/
cp -r ./digital_publishing_workflow_tools/workflows/storage_resource_retriever/storage_resource_retriever_1/* ./htx_1_windows/workflows/storage_resource_retriever/storage_resource_retriever_1/
cp -r -n ./scripts_windows/workflows/storage_resource_retriever/storage_resource_retriever_1/* ./htx_1_windows/workflows/storage_resource_retriever/storage_resource_retriever_1/
mkdir -p ./htx_1_windows/workflows/resource_retriever/resource_retriever_1/
cp -r ./digital_publishing_workflow_tools/workflows/resource_retriever/resource_retriever_1/* ./htx_1_windows/workflows/resource_retriever/resource_retriever_1/
cp -r -n ./scripts_windows/workflows/resource_retriever/resource_retriever_1/* ./htx_1_windows/workflows/resource_retriever/resource_retriever_1/
mkdir -p ./htx_1_windows/http_client/http_client_1/
cp -r ./digital_publishing_workflow_tools/http_client/http_client_1/* ./htx_1_windows/http_client/http_client_1/
cp -r -n ./scripts_windows/http_client/http_client_1/* ./htx_1_windows/http_client/http_client_1/
mkdir -p ./htx_1_windows/https_client/https_client_1/
cp -r ./digital_publishing_workflow_tools/https_client/https_client_1/* ./htx_1_windows/https_client/https_client_1/
cp -r -n ./scripts_windows/https_client/https_client_1/* ./htx_1_windows/https_client/https_client_1/
mkdir -p ./htx_1_windows/xml_namespace_extractor/xml_namespace_extractor_1/
cp -r ./digital_publishing_workflow_tools/xml_namespace_extractor/xml_namespace_extractor_1/* ./htx_1_windows/xml_namespace_extractor/xml_namespace_extractor_1/
cp -r -n ./scripts_windows/xml_namespace_extractor/xml_namespace_extractor_1/* ./htx_1_windows/xml_namespace_extractor/xml_namespace_extractor_1/
mkdir -p ./htx_1_windows/xml_xslt_transformator/xml_xslt_transformator_1/
cp -r ./digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/* ./htx_1_windows/xml_xslt_transformator/xml_xslt_transformator_1/
cp -r -n ./scripts_windows/xml_xslt_transformator/xml_xslt_transformator_1/* ./htx_1_windows/xml_xslt_transformator/xml_xslt_transformator_1/
mkdir -p ./htx_1_windows/gui/list_picker/list_picker_1/
cp -r ./digital_publishing_workflow_tools/gui/list_picker/list_picker_1/* ./htx_1_windows/gui/list_picker/list_picker_1/
cp -r ./digital_publishing_workflow_tools/workflows/storage_resource_retriever/storage_resource_retriever_1/* ./htx_1_windows/workflows/storage_resource_retriever/storage_resource_retriever_1/
zip -r htx_1_windows_$currentDate.zip htx_1_windows
sha256sum htx_1_windows_$currentDate.zip > htx_1_windows_$currentDate.zip.sha256
rm -r ./htx_1_windows/


rm -r ./tero-grammars/
rm -r ./jterosta/
rm -r ./digital_publishing_workflow_tools/
